<div class="section">
	<div class="container">

		<div class="topsearchwrap">
				
				<div class="topbar-search">
					<div class="browse-lawyer">
						<h2>Browse Lawyers</h2>
					</div>
					<ul>
						<li class="tab-law active" data-tab="practice_area_tab">
							<h3>Browse By Practice Area</h3>
						</li>
						<li class="tab-law" data-tab="cities_tab">
							<h3>Browse By Cities</h3>
						</li>
						<li class="tab-law" data-tab="browse_by_categories_tab">
							<h3>Browse By Categories</h3>
						</li>
					</ul>
				</div>
				<div class="srchbx">				
				<!--Categories start-->
				{{-- <h4>{{__('Browse By Practice Area')}}</h4> --}}
				<div class="srchint">
					<div class="main-srchint">
						<img src="{{asset('images/hamer.png')}}" alt="">
					</div>
					<ul class="row catelist tab active" id="practice_area_tab">
						@if(isset($topFunctionalAreaIds) && count($topFunctionalAreaIds)) @foreach($topFunctionalAreaIds as $functional_area_id_num_jobs)
						<?php
						$functionalArea = App\ FunctionalArea::where( 'functional_area_id', '=', $functional_area_id_num_jobs->functional_area_id )->lang()->active()->first();
						?> @if(null !== $functionalArea)
						
						<li class="col-md-4 col-sm-6"><a href="{{route('job.list', ['functional_area_id[]'=>$functionalArea->functional_area_id])}}" title="{{$functionalArea->functional_area}}">{{$functionalArea->functional_area}} <span>({{$functional_area_id_num_jobs->num_jobs}})</span></a>
						</li>
						
						@endif @endforeach @endif
					</ul>
					<ul class="row catelist tab" id="cities_tab">

						@if(isset($topCityIds) && count($topCityIds)) @foreach($topCityIds as $city_id_num_jobs)

						<?php

						$city = App\ City::getCityById( $city_id_num_jobs->city_id );
						
						?> @if(null !== $city)
						
						<li class="col-md-4 col-xs-6"><a href="{{route('job.list', ['city_id[]'=>$city->city_id])}}" title="{{$city->city}}">{{$city->city}} <span>({{$city_id_num_jobs->num_jobs}})</span></a>
						</li>
						
						@endif @endforeach @endif
					</ul>
					<ul class="row catelist tab" id="browse_by_categories_tab">					
						@if(isset($topIndustryIds) && count($topIndustryIds)) @foreach($topIndustryIds as $industry_id => $num_jobs)

						<?php

						$industry = App\ Industry::where( 'industry_id', '=', $industry_id )->lang()->active()->first();

						?> @if(null !== $industry)

						<li class="col-md-4 col-sm-6"><a href="{{route('job.list', ['industry_id[]'=>$industry->industry_id])}}" title="{{$industry->industry}}">{{$industry->industry}} <span>({{$num_jobs}})</span></a>
						</li>


						@endif @endforeach @endif
					</ul>
					<!--Categories end-->
				</div>
				</div>
				


	</div>
</div>
</div>